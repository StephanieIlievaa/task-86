# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣
# A JavaScript Task
In this task we have the opportunity to exercise your skills with array filter method and also uninstalling modules. After we have filterred the array we have to display its elements in a browser.
## Objective
* Checkout the dev branch.
* Uninstall the ramda package using npm 
* Replace the provided ramda filter method with the native JavaScript array filter and use it on the numbers array
* In the provided **ul** we have to append **li** which represents each nimber from the array.
* When implemented merge the dev branch into master.


## Requirements
* The project starts with **npm run start**.
* The ramda package should be unin stalled and its dependency should be removed from the **package.json** file.
* We have to create a list items for each **even number** of the filtered array and to add the current number as text to a list item.
* Finally, we have to append each item to the unordered list.
